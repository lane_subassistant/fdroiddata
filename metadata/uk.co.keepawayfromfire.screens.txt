Categories:System
License:SimplifiedBSD
Web Site:http://keepawayfromfire.co.uk/screens.html
Source Code:https://github.com/Cj-Malone/Screens
Issue Tracker:https://github.com/Cj-Malone/Screens/issues
Changelog:https://raw.githubusercontent.com/Cj-Malone/Screens/HEAD/CHANGELOG.md

Auto Name:Screens
Summary:Multi Window Manager
Description:
Automatically enter split screen mode from launcher shortcuts and Quick Settings
tiles.
.

Repo Type:git
Repo:https://github.com/Cj-Malone/Screens
Binaries:https://github.com/Cj-Malone/Screens/releases/download/v%c/uk.co.keepawayfromfire.screens-%c.apk

Build:4,4
    commit=v4
    subdir=app
    gradle=yes

Build:5,5
    commit=v5
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:v5
Current Version Code:5
